package com.moizc.moviereviewservice.integration;

import com.moizc.moviereviewservice.dto.MovieReviewDto;
import com.moizc.moviereviewservice.dto.UpdateMovieReviewRequestDto;
import com.moizc.moviereviewservice.model.MovieReview;
import com.moizc.moviereviewservice.repository.MovieReviewRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWebTestClient
public class MovieReviewRouterIntegrationTest {

    @Autowired
    private WebTestClient webTestClient;

    @Autowired
    private MovieReviewRepository movieReviewRepository;

    @AfterEach
    void tearDown() {
        movieReviewRepository.deleteAll().block();
    }

    @Test
    void shouldAddAReview() {
        //arrange
        MovieReviewDto movieReviewDto = MovieReviewDto.builder()
                .movieInfoId("1fa")
                .comment("Loved it!")
                .rating(4.3)
                .reviewerId("abc")
                .build();

        //act
        webTestClient.post().uri("/v1/movie-review")
                .bodyValue(movieReviewDto)
                .exchange()
                .expectStatus().isCreated()
                .expectBody(MovieReviewDto.class)
                .consumeWith(movieReviewDtoEntityExchangeResult -> {
                    MovieReviewDto responseBody = movieReviewDtoEntityExchangeResult.getResponseBody();
                    Assertions.assertEquals(movieReviewDto, responseBody);
                });
    }

    @Test
    void shouldGetAllMovieReviews() {
        //arrange
        String movieInfoId = "1fa";
        MovieReview movieReview1 = MovieReview.builder()
                .movieInfoId(movieInfoId)
                .comment("Loved it!")
                .rating(4.3)
                .reviewerId("abc")
                .build();

        MovieReview movieReview2 = MovieReview.builder()
                .movieInfoId(movieInfoId)
                .comment("Boring")
                .rating(1.0)
                .reviewerId("xyz")
                .build();

        movieReviewRepository.saveAll(List.of(movieReview1, movieReview2)).blockFirst();

        //act
        webTestClient.get().uri("/v1/movie-reviews/" + movieInfoId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(MovieReviewDto.class)
                .consumeWith(movieReviewDtoEntityExchangeResult -> {
                    List<MovieReviewDto> responseBody = movieReviewDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;

                    Assertions.assertEquals(2, responseBody.size());

                    MovieReviewDto movieReviewDto1 = responseBody.get(0);
                    MovieReviewDto movieReviewDto2 = responseBody.get(1);

                    Assertions.assertEquals(movieReview1.getReviewerId(), movieReviewDto1.reviewerId());
                    Assertions.assertEquals(movieReview1.getRating(), movieReviewDto1.rating());
                    Assertions.assertEquals(movieReview1.getMovieInfoId(), movieReviewDto1.movieInfoId());
                    Assertions.assertEquals(movieReview1.getComment(), movieReviewDto1.comment());

                    Assertions.assertEquals(movieReview2.getReviewerId(), movieReviewDto2.reviewerId());
                    Assertions.assertEquals(movieReview2.getRating(), movieReviewDto2.rating());
                    Assertions.assertEquals(movieReview2.getMovieInfoId(), movieReviewDto2.movieInfoId());
                    Assertions.assertEquals(movieReview2.getComment(), movieReviewDto2.comment());
                });
    }

    @Test
    void shouldUpdateMovieReview() {
        //arrange
        String movieReviewId = "pqr";
        MovieReview movieReview1 = MovieReview.builder()
                .id(movieReviewId)
                .movieInfoId("1fa")
                .comment("Loved i!")
                .rating(4.3)
                .reviewerId("abc")
                .build();

        movieReviewRepository.save(movieReview1).block();

        UpdateMovieReviewRequestDto updateMovieReviewRequestDto = UpdateMovieReviewRequestDto.builder()
                .comment("Loved it!")
                .rating(5.0)
                .build();

        //act
        webTestClient.patch().uri("/v1/movie-review/" + movieReviewId)
                .bodyValue(updateMovieReviewRequestDto)
                .exchange()
                .expectStatus().isOk()
                .expectBody(MovieReviewDto.class)
                .consumeWith(movieReviewDtoEntityExchangeResult -> {
                    MovieReviewDto responseBody = movieReviewDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;

                    Assertions.assertEquals(movieReview1.getReviewerId(), responseBody.reviewerId());
                    Assertions.assertEquals(updateMovieReviewRequestDto.rating(), responseBody.rating());
                    Assertions.assertEquals(movieReview1.getMovieInfoId(), responseBody.movieInfoId());
                    Assertions.assertEquals(updateMovieReviewRequestDto.comment(), responseBody.comment());
                });
    }

    @Test
    void shouldDeleteMovieReview() {
        //arrange
        String movieReviewId = "pqr";
        MovieReview movieReview1 = MovieReview.builder()
                .id(movieReviewId)
                .movieInfoId("1fa")
                .comment("Loved i!")
                .rating(4.3)
                .reviewerId("abc")
                .build();

        movieReviewRepository.save(movieReview1).block();

        //act
        webTestClient.delete().uri("/v1/movie-review/" + movieReviewId)
                .exchange()
                .expectStatus().isNoContent();
    }

    @Test
    void shouldThrowErrorWhenMovieReviewIsNotFoundForUpdatingAMovieReview() {
        //arrange
        String movieReviewId = "pqr";

        UpdateMovieReviewRequestDto updateMovieReviewRequestDto = UpdateMovieReviewRequestDto.builder()
                .comment("Loved it!")
                .rating(5.0)
                .build();

        //act
        webTestClient.patch().uri("/v1/movie-review/" + movieReviewId)
                .bodyValue(updateMovieReviewRequestDto)
                .exchange()
                .expectStatus()
                .isNotFound()
                .expectBody(String.class)
                .consumeWith(movieReviewDtoEntityExchangeResult -> {
                    String responseBody = movieReviewDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                    Assertions.assertEquals("Movie review not found", responseBody);
                });
    }
}
