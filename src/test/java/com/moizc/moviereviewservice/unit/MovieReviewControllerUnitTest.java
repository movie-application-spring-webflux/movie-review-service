package com.moizc.moviereviewservice.unit;

import com.moizc.moviereviewservice.dto.MovieReviewDto;
import com.moizc.moviereviewservice.dto.UpdateMovieReviewRequestDto;
import com.moizc.moviereviewservice.exception.ErrorHandler;
import com.moizc.moviereviewservice.handler.MovieRouterHandler;
import com.moizc.moviereviewservice.model.MovieReview;
import com.moizc.moviereviewservice.repository.MovieReviewRepository;
import com.moizc.moviereviewservice.router.MovieReviewRouter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@WebFluxTest
@AutoConfigureWebTestClient
@ContextConfiguration(classes = {MovieReviewRouter.class, MovieRouterHandler.class, ErrorHandler.class})
public class MovieReviewControllerUnitTest {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private MovieReviewRepository movieReviewRepository;

    @Test
    void shouldGetAllMovieReviews() {
        //arrange
        String movieInfoId = "1fa";
        MovieReview movieReview1 = MovieReview.builder()
                .movieInfoId(movieInfoId)
                .comment("Loved it!")
                .rating(4.3)
                .reviewerId("abc")
                .build();

        MovieReview movieReview2 = MovieReview.builder()
                .movieInfoId(movieInfoId)
                .comment("Boring")
                .rating(1.0)
                .reviewerId("xyz")
                .build();

        when(movieReviewRepository.findByMovieInfoId(isA(String.class))).thenReturn(Flux.just(movieReview1, movieReview2));

        //act
        webTestClient.get().uri("/v1/movie-reviews/" + movieInfoId)
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(MovieReviewDto.class)
                .consumeWith(movieReviewDtoEntityExchangeResult -> {
                    List<MovieReviewDto> responseBody = movieReviewDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;

                    Assertions.assertEquals(2, responseBody.size());
                });
    }

    @Test
    void shouldUpdateMovieReview() {
        //arrange
        String movieReviewId = "pqr";
        MovieReview movieReview = MovieReview.builder()
                .id(movieReviewId)
                .movieInfoId("1fa")
                .comment("Loved i!")
                .rating(4.3)
                .reviewerId("abc")
                .build();

        MovieReview updatedMovieReview = MovieReview.builder()
                .id(movieReviewId)
                .movieInfoId("1fa")
                .comment("Loved it!")
                .rating(5.0)
                .reviewerId("abc")
                .build();

        when(movieReviewRepository.findById(isA(String.class))).thenReturn(Mono.just(movieReview));
        when(movieReviewRepository.save(isA(MovieReview.class))).thenReturn(Mono.just(updatedMovieReview));

        UpdateMovieReviewRequestDto updateMovieReviewRequestDto = UpdateMovieReviewRequestDto.builder()
                .comment("Loved it!")
                .rating(5.0)
                .build();

        //act
        webTestClient.patch().uri("/v1/movie-review/" + movieReviewId)
                .bodyValue(updateMovieReviewRequestDto)
                .exchange()
                .expectStatus().isOk()
                .expectBody(MovieReviewDto.class)
                .consumeWith(movieReviewDtoEntityExchangeResult -> {
                    MovieReviewDto responseBody = movieReviewDtoEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                });
    }

    @Test
    void shouldDeleteMovieReview() {
        //arrange
        String movieReviewId = "pqr";

        when(movieReviewRepository.deleteById(movieReviewId)).thenReturn(Mono.empty());

        //act
        webTestClient.delete().uri("/v1/movie-review/" + movieReviewId)
                .exchange()
                .expectStatus().isNoContent();
    }

    @Test
    void shouldValidateMovieReviewDtoWhenAddingAMovieReview() {
        //arrange
        MovieReviewDto movieReviewDto = MovieReviewDto.builder()
                .movieInfoId(null)
                .comment("Loved it!")
                .rating(-4.3)
                .reviewerId("abc")
                .build();

        //act
        webTestClient.post().uri("/v1/movie-review")
                .bodyValue(movieReviewDto)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class)
                .consumeWith(stringEntityExchangeResult -> {
                    String responseBody = stringEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                    Assertions.assertEquals("Movie info ID is needed to add a review,rating cannot be negative", responseBody);
                });
    }

    @Test
    void shouldValidateUpdateMovieReviewDtoWhenUpdatingAMovieReview() {
        //arrange
        MovieReviewDto movieReviewDto = MovieReviewDto.builder()
                .movieInfoId("1fa")
                .comment("Loved it!")
                .rating(-4.3)
                .reviewerId("abc")
                .build();

        //act
        webTestClient.post().uri("/v1/movie-review")
                .bodyValue(movieReviewDto)
                .exchange()
                .expectStatus()
                .isBadRequest()
                .expectBody(String.class)
                .consumeWith(stringEntityExchangeResult -> {
                    String responseBody = stringEntityExchangeResult.getResponseBody();
                    assert responseBody != null;
                    Assertions.assertEquals("rating cannot be negative", responseBody);
                });
    }

}
