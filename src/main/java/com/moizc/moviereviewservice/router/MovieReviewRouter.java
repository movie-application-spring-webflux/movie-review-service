package com.moizc.moviereviewservice.router;

import com.moizc.moviereviewservice.handler.MovieRouterHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Configuration
public class MovieReviewRouter {

    @Bean
    public RouterFunction<ServerResponse> movieReviewsRoute(MovieRouterHandler movieRouterHandler) {

        return route()
                .POST("/v1/movie-review", movieRouterHandler::addMovieReview)
                .GET("/v1/movie-reviews/{movie_info_id}", movieRouterHandler::getAllMovieReviews)
                .PATCH("/v1/movie-review/{movie_review_id}", movieRouterHandler::updateMovieReview)
                .DELETE("/v1/movie-review/{movie_review_id}", movieRouterHandler::deleteMovieReview)
                .build();
    }
}
