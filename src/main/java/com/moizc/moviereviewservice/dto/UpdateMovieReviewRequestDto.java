package com.moizc.moviereviewservice.dto;

import jakarta.validation.constraints.Min;
import lombok.Builder;

@Builder
public record UpdateMovieReviewRequestDto(String comment,
                                          @Min(value = 0, message = "rating cannot be negative")
                                          Double rating) {
}
