package com.moizc.moviereviewservice.dto;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import lombok.Builder;

@Builder
public record MovieReviewDto(
        @NotBlank(message = "Movie info ID is needed to add a review")
        String movieInfoId,
        String comment,
        @Min(value = 0, message = "rating cannot be negative")
        Double rating,
        String reviewerId) {
}
