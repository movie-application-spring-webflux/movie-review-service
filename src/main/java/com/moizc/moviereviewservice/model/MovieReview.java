package com.moizc.moviereviewservice.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document
public class MovieReview {

    @Id
    private String id;
    private String movieInfoId;
    private Double rating;
    private String comment;
    private String reviewerId;
}
