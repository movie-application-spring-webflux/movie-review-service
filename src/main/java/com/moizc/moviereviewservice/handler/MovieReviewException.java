package com.moizc.moviereviewservice.handler;

public class MovieReviewException extends RuntimeException {

    public MovieReviewException(String message) {
        super(message);
    }
}
