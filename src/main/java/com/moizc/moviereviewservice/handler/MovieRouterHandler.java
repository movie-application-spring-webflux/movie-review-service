package com.moizc.moviereviewservice.handler;

import com.moizc.moviereviewservice.dto.MovieReviewDto;
import com.moizc.moviereviewservice.dto.UpdateMovieReviewRequestDto;
import com.moizc.moviereviewservice.exception.ValidationException;
import com.moizc.moviereviewservice.model.MovieReview;
import com.moizc.moviereviewservice.repository.MovieReviewRepository;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class MovieRouterHandler {

    private final MovieReviewRepository movieReviewRepository;
    private final Validator validator;

    public MovieRouterHandler(MovieReviewRepository movieReviewRepository, Validator validator) {
        this.movieReviewRepository = movieReviewRepository;
        this.validator = validator;
    }

    public Mono<ServerResponse> addMovieReview(ServerRequest request) {
        return request.bodyToMono(MovieReviewDto.class)
                .doOnNext(this::validateMovieReviewDto)
                .map(this::getMovieReviewFromDto)
                .flatMap(movieReviewRepository::save)
                .flatMap(movieReview -> ServerResponse.status(HttpStatus.CREATED).bodyValue(movieReview));
    }

    private MovieReview getMovieReviewFromDto(MovieReviewDto movieReviewDto) {
        return MovieReview.builder()
                .rating(movieReviewDto.rating())
                .comment(movieReviewDto.comment())
                .movieInfoId(movieReviewDto.movieInfoId())
                .reviewerId(movieReviewDto.reviewerId())
                .build();
    }

    public Mono<ServerResponse> getAllMovieReviews(ServerRequest request) {
        String movieInfoId = request.pathVariable("movie_info_id");
        Flux<MovieReviewDto> movieReviewsDtoFlux = movieReviewRepository.findByMovieInfoId(movieInfoId)
                .map(this::getMovieReviewDtoFromEntity).log();
        return ServerResponse.status(HttpStatus.OK).body(movieReviewsDtoFlux, MovieReviewDto.class);
    }

    private MovieReviewDto getMovieReviewDtoFromEntity(MovieReview movieReview) {
        return MovieReviewDto.builder()
                .reviewerId(movieReview.getReviewerId())
                .rating(movieReview.getRating())
                .comment(movieReview.getComment())
                .movieInfoId(movieReview.getMovieInfoId())
                .build();
    }

    public Mono<ServerResponse> updateMovieReview(ServerRequest request) {
        String movieReviewId = request.pathVariable("movie_review_id");

        Mono<UpdateMovieReviewRequestDto> updateMovieReviewRequestDtoMono = request.bodyToMono(UpdateMovieReviewRequestDto.class)
                .doOnNext(this::validateUpdateMovieReviewDto);

        Mono<MovieReview> movieReviewMono = movieReviewRepository.findById(movieReviewId)
                .switchIfEmpty(Mono.error(new MovieReviewException("Movie review not found")));

        return movieReviewMono
                .flatMap(movieReview -> updateMovieReviewRequestDtoMono.map(updateMovieReview -> {
                    movieReview.setComment(updateMovieReview.comment());
                    movieReview.setRating(updateMovieReview.rating());
                    return movieReview;
                }))
                .flatMap(movieReviewRepository::save)
                .map(this::getMovieReviewDtoFromEntity)
                .flatMap(movieReviewDto -> ServerResponse.status(HttpStatus.OK).bodyValue(movieReviewDto));
    }


    public Mono<ServerResponse> deleteMovieReview(ServerRequest request) {
        String movieReviewId = request.pathVariable("movie_review_id");

        return movieReviewRepository.deleteById(movieReviewId)
                .then(ServerResponse.status(HttpStatus.NO_CONTENT).build());
    }

    private void validateMovieReviewDto(MovieReviewDto movieReviewDto) {
        Set<ConstraintViolation<MovieReviewDto>> constraintViolations = validator.validate(movieReviewDto);
        log.info("Constraint violations: {}", constraintViolations);
        if (constraintViolations.size() > 0) {
            String constraintViolation = constraintViolations.stream().map(ConstraintViolation::getMessage)
                    .sorted().collect(Collectors.joining(","));
            throw new ValidationException(constraintViolation);
        }
    }

    private void validateUpdateMovieReviewDto(UpdateMovieReviewRequestDto updateMovieReviewRequestDto) {
        Set<ConstraintViolation<UpdateMovieReviewRequestDto>> constraintViolations = validator.validate(updateMovieReviewRequestDto);
        log.info("Constraint violations: {}", constraintViolations);
        if (constraintViolations.size() > 0) {
            String constraintViolation = constraintViolations.stream().map(ConstraintViolation::getMessage)
                    .sorted().collect(Collectors.joining(","));
            throw new ValidationException(constraintViolation);
        }
    }
}
